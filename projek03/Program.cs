﻿// Bartłomiej Kaftan
// projekt 2 grafy
// 6 maj 2015

using System;
using System.Collections.Generic;
using System.Linq;


namespace Projekt
{
	class Program
	{
		static List<int>[] miasto;
		// tablica przechowująca listy intów
		
		static List<int> dywersanci;
		// lista dywersantów
		
		public static void Main(string[] args)
		{
			bool czy = true;
					
			if (czy) {
			
				miasto = new List<int>[9];
				for (int a = 0; a < miasto.Length; ++a)
					miasto[a] = new List<int>(); //zapełnienie tablicy pustymi listami intów
				DodajKrawedz(miasto, 1, 2, true);
				DodajKrawedz(miasto, 2, 4, true);
				DodajKrawedz(miasto, 1, 3, true);
				DodajKrawedz(miasto, 3, 4, true); // dodawanie przecznic
				DodajKrawedz(miasto, 4, 5, true);
				DodajKrawedz(miasto, 5, 6, true);
				DodajKrawedz(miasto, 6, 7, true);
				DodajKrawedz(miasto, 7, 8, true);
				DodajKrawedz(miasto, 5, 8, true);
				DodajKrawedz(miasto, 8, 9, true);
								
				dywersanci = new List<int>(){ 0, 6 }; // dywersanci siedzą na pozycjach podanych + 1
			
			} else
				Wejscie(ref miasto, ref dywersanci);  // pobiera wejście
			
			
			
			// pętla wyświetla newralgiczne przecznice przelatując przez liste krotek
			// zwróconych przez metode ZnajdzNewralgicznePrzecznice
			foreach (Tuple<int,int> tuple in ZnajdzNewralgicznePrzecznice(miasto))
				Console.WriteLine("Newralgiczne przecznice: " + (tuple.Item1 + 1) + " " + (tuple.Item2 + 1));
			// zmienne na potrzeby odnalezienia najbliższego newralgicznego przejścia
			int najblizszyDywersant = 0, najblizszyWierzcholekNewralgicznegoMostu = 0, maxOdleglosc = int.MaxValue, jakDaleko;
			// wypisanie odległości jaką musi pokonać poszczególny dywersant aby osiągnąć daną przecznicę
			foreach (int a in dywersanci)
				for (int b = 0; b < miasto.Length; ++b) {
					jakDaleko = Odleglosc(miasto, a, b);
					Console.WriteLine("Odleglosc: " + (a + 1) + " " + (b + 1) + ": " + jakDaleko);
				
					// lokalizacja dywersanta, który będzie miał najbliżej do newralgicznego mostu
					foreach (Tuple<int,int> tuple in ZnajdzNewralgicznePrzecznice(miasto)) {
						if (jakDaleko < maxOdleglosc && (b + 1 == tuple.Item1 + 1 || b + 1 == tuple.Item2 + 1)) {
							maxOdleglosc = jakDaleko;
							najblizszyDywersant = a + 1;
							najblizszyWierzcholekNewralgicznegoMostu = b + 1;
						}
					}
				}
			// info w postaci: pozycja najbliższego dywersanta - najbliższy wierzchołek newralgicznego mostu - odległość do pokonania
			Console.WriteLine("\nDywersant najbliżej newralgicznego mostu i wierzcholek tego mostu:\n " + najblizszyDywersant + " " + najblizszyWierzcholekNewralgicznegoMostu +
			" odl: " + maxOdleglosc + "\n");
			
			// możliwe trasy dywersantów po mieście
			foreach (int a in dywersanci)
				for (int b = 0; b < miasto.Length; ++b)
					Console.WriteLine("Trasa: " + (a + 1) + " ==>> " + (b + 1) + ": " + Trasa(miasto, a, b));
			
			
			Console.WriteLine("\n#####################################################");
			Console.WriteLine("Dywersant startujący w wierzchołku nr: " + najblizszyDywersant
			+ " wysadza wierzchołek " + najblizszyWierzcholekNewralgicznegoMostu +
			".\nIdzie jak poniżej:");
			// który dywersant wysadzi który wierzchołek
			foreach (int a in dywersanci)
				for (int b = 0; b < miasto.Length; ++b)
					if (b + 1 == najblizszyWierzcholekNewralgicznegoMostu && a + 1 == najblizszyDywersant) {
						Console.WriteLine("Trasa: " + (a + 1) + " ==>> " + (b + 1) + ": " + Trasa(miasto, a, b));
						// odległość dywersanta do wierzchołka wysadzanego
						Console.WriteLine("Dywersant nr {0} przemierzył w ten sposób odległość = {1}",
							najblizszyDywersant, ((Trasa(miasto, a, b)).Split('-')).Length - 1);		
					}
			// wskazanie który most zaprzestał swego istnienia
			foreach (Tuple<int,int> tuple in ZnajdzNewralgicznePrzecznice(miasto)) {
				if (najblizszyWierzcholekNewralgicznegoMostu == tuple.Item1 + 1 || najblizszyWierzcholekNewralgicznegoMostu == tuple.Item2 + 1) {
					Console.WriteLine("Pozbyto się mostu: {0}-{1} - najbliższej newralgicznej dzielnicy.", najblizszyWierzcholekNewralgicznegoMostu,
						(najblizszyWierzcholekNewralgicznegoMostu == tuple.Item1 + 1) ? tuple.Item2 + 1 : tuple.Item1 + 1);
				}
			}
			Console.WriteLine("#####################################################");
			Console.WriteLine("Dodatkowe: ");
			// lista krotek zawierająca wierzchołki newralgicznych mostów
			List<Tuple<int,int>> list = ZnajdzNewralgicznePrzecznice(miasto);
			// list2 przechowuje newralgiczne wierzchołki
			List<int> list2 = new List<int>();
			foreach (Tuple<int,int> tuple in list) {
				list2.Add(tuple.Item1);
				list2.Add(tuple.Item2);
			}
			// pętla foreach przchodząca po kolekcji dywersantów
			foreach (var w in dywersanci) {
				Console.WriteLine("Dywersant startujący z: " + (w + 1)); // wskazanie konkretnego dywersanta
				trasa = new List<int>(); // tutaj zapisze się jego trasa
				koniecFunkcji = false; // nowa trasa, więc zmienna boolowska idzie na false
				// poniżej w funkcji ZnajdzCalaTrase uzupełniana jest tablica krotek oznaczających numery 
				//odwiedzanych wierzchołków na podstawie zadanych parametrów
				ZnajdzCalaTrase(miasto, (w), ZnajdzNewralgicznePrzecznice(miasto));
				// w poniższym forze wypisujemy trasę dla poszczególnych dywersantów
				for (int t = 0; t < trasa.Count; ++t)
					Console.WriteLine((trasa[t] + 1) + " ");
					
				
				Console.WriteLine("Spr:");
				for (int tt = 0; tt < trasa.Count; ++tt) {
					if (tt != trasa.Count - 1 && list2.Contains(trasa[tt]) && list2.Contains(trasa[tt + 1])
					    && list.Any(a => a.Item1 == trasa[tt]// jesli w list istnieje element o właściwościach a.Item1 == trasa[tt]
					    && a.Item2 == trasa[tt + 1])) {
						Console.WriteLine((trasa[tt] + 1) + " " + (trasa[tt + 1] + 1) + " BUM ");
						++tt;
					} else if (!list2.Contains(trasa[tt])) {
						Console.WriteLine((trasa[tt] + 1) + " ");
					} else if (list2.Contains(trasa[tt])) {
						Console.WriteLine((trasa[tt] + 1) + " BUM ");
					}
				}
			}

			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
		
		// metoda do wprowadzania danych wejściowych
		public static void Wejscie(ref List<int>[] m, ref List<int> d)
		{
			Console.WriteLine("Podaj dane: liczba wierzchołków, w nowych liniach połączenia między nimi w formacie np. 1-2, dalej "
			+ "liczbę dywersantów, i dalej w nowych liniach ich rozmieszczenie");
			int w = int.Parse(Console.ReadLine());
			m = new List<int>[w];
			d = new List<int>();
			for (int a = 0; a < m.Length; ++a)
				m[a] = new List<int>();
			int f, s;
			int stoper = 1;
			while (stoper == 1) {
				string word = Console.ReadLine();
				string[] words = word.Split('-');
				if (words.LongLength == 2) {
					f = int.Parse(words[0]);
					s = int.Parse(words[1]);
					DodajKrawedz(m, f, s, true);
				}
				if (words.Length == 1) {
					for (int i = 0; i < int.Parse(words[0]); i++) {
						d.Add(int.Parse(Console.ReadLine()) - 1);
					}
					stoper = 0;
				}
			}
		}
		
		// metoda usuwająca krawędź z grafu podanego jako pierwszy parametr, o zadanych krawędziach
		public static void UsunKrawedz(List<int>[] graf, int od, int do_)
		{
			graf[od].Remove(do_);
			graf[do_].Remove(od);
		}
		
		// dodanie krawędzi
		public static void DodajKrawedz(List<int>[] graf, int od, int do_, bool minusJeden = false)
		{
			if (minusJeden) {
				--od;
				--do_;
			}
			graf[od].Add(do_);
			graf[do_].Add(od);
		}
		
		// metoda znajdująca newralgiczne przecznice, jako parametr przyjmuje tablice list a zwraca liste krotek
		public static List<Tuple<int,int>> ZnajdzNewralgicznePrzecznice(List<int>[] graf)
		{
			List<Tuple<int,int>> lista = new List<Tuple<int, int>>();
			for (int a = 0; a < graf.Length; ++a) // pętla zewnętrzna przechodzi po grafie
				for (int b = 0; b < graf.Length; ++b) { // podobnie pętla zagnieżdżona
					if (graf[a].Contains(b) && a < b) { // czy krawędź a->b jest w grafie, jak w grafie nieskierowanym występuje ab, to i ba tez
						UsunKrawedz(graf, a, b); // usuwa zadaną krawędź
						if (GrafNieJestSpojny(graf, 0))
							lista.Add(new Tuple<int,int>(a, b)); // dodanie do listy krotki
						DodajKrawedz(graf, a, b); // dodajemy ponownie wcześniej usuniętą krawędź
					}
				}
			return lista;
		}
		/*
		 funkcja BreadthFirstSearch (Graf G, Wierzchołek s)
    	dla każdego wierzchołka u z G:
        kolor[u] = biały
        odleglosc[u] = inf
        rodzic[u] = NIL
	    kolor[s] = SZARY
	    odleglosc[s] = 0
	    rodzic[s] = NIL
	    Q.push(s)
	    dopóki kolejka Q nie jest pusta:
        u = Q.pop()
        dla każdego v z listy sąsiedztwa u:
            jeżeli v jest biały:
                kolor[v] = SZARY
                odleglosc[v] = odleglosc[u] + 1
                rodzic[v] = u
                Q.push(v)
        kolor[u] = CZARNY
		
		Danymi wejściowymi dla algorytmu jest reprezentacja grafu w postaci listy sąsiedztwa
wierzchołków oraz wierzchołek od którego rozpoczynane jest przeszukiwanie. Początkowo wszystkie
wierzchołki kolorowane są na biało, co oznacza, że nie zostały jeszcze odwiedzone. Inicjalizowane
 jest także pole odległości, które zawiera informacje o odległości danego wierzchołka od s oraz pole
rodzica, które jest wykorzystywane przy odtwarzaniu drzewa przeszukiwania wszerz. Kolejka FIFO Q jest
 inicjalizowana węzłem startowym, którego kolor ustawiony został na szaro – oznacza to, że węzeł został
 już odwiedzony, lecz nie zostały odwiedzone węzły do niego sąsiednie. Następnie, pobierany jest pierwszy
 wierzchołek z kolejki i analizowana jest lista jego sąsiadów. Jeżeli sąsiad jest biały, oznacza to, że nie
 został jeszcze odwiedzony: aktualizowane są więc pola odległości i rodzica oraz jego kolor a następnie jest
 on dodawany do kolejki. Po przeglądnięciu wszystkich sąsiadów danego węzła kolor węzła bieżącego zmieniany jest
 na czarny (wszyscy jego sąsiedzi zostali odwiedzeni) i operacja powtarza się dla następnego węzła znajdującego się
 w kolejce, bądź też (jeżeli kolejka jest pusta) algorytm kończy swoje działanie.
		 */
		
		static public bool GrafNieJestSpojny(List<int>[] graf, int start)//http://pl.wikipedia.org/wiki/Przeszukiwanie_wszerz
		{
			if (graf[start].Count == 0) // brak sąsiadów, zwracamy prawdę
				return true;
			Kolor[] kolor = new Program.Kolor[graf.Length]; // tablica zawierająca kolory
			System.Collections.Generic.Stack<int> stack = new Stack<int>(); // powołanie do istenienia stosu intów
			for (int a = 0; a < graf.Length; ++a) // pętla przechodzi po grafie
				kolor[a] = Kolor.Bialy; // na początek wierzchołki kolorowane są na biało
			kolor[start] = Kolor.Szary; // węzeł startowy zakolorowany na szaro
			stack.Push(start); // na stos idzie pierwszy wierzchołek
			while (stack.Count > 0) {
				int u = stack.Pop(); // bierzemy pierwszy wierzchołek ze stosu
				foreach (int v in graf[u]) { // analizujemy liste jego sąsiadów
					if (kolor[v] == Kolor.Bialy) { // biały sąsiad to nie odwiedzony sąsiad
						kolor[v] = Kolor.Szary; // zmiana jego koloru na szary, czyli już odwiedzony
						stack.Push(v); // włożenie odwiedzonego sąsiada na stos
					}
				}
				kolor[u] = Kolor.Czarny; // odwiedzono wszystkich sąsiadów to kolor węzła zmienia się na czarny
			}
			
			foreach (Kolor k in kolor) // przechodzimy pętlą po kolekcji kolor
				if (k != Kolor.Czarny) // jeżeli k będzie nie czarne zwracamy prawdę
					return true;
			return false;	// gdy powyższy if nie zajdzie to zwracamy fałsz
		}
		
		// także na podstawie algorytmu przeszukiwania wszerz jak wyżej opisano
		// funkcja zwraca odleglość wierzchołków od siebie
		static public int Odleglosc(List<int>[] graf, int start, int koniec)
		{
			Kolor[] kolor = new Program.Kolor[graf.Length]; // tablica kolorów o rozmiarze tablicy graf
			int[] odleglosc = new int[graf.Length]; // tablica odległości
			System.Collections.Generic.Stack<int> stack = new Stack<int>(); //jw.
			for (int a = 0; a < graf.Length; ++a) { //jw.
				kolor[a] = Kolor.Bialy;
				odleglosc[a] = int.MaxValue; // przypisanie wartości początkowych do tablicy ogległości
			}
			kolor[start] = Kolor.Szary; // do tablicy kolor na miejscu start przypisujemy kolor szary
			odleglosc[start] = 0; // odległość w tym miejscu to 0
			stack.Push(start); // wkładamy na stos dany wierzchołek
			while (stack.Count > 0) { // działamy dopóty, dopóki stos nie będzie pusty
				int u = stack.Pop(); // zdjęcie ze stosu wartości i przypisanie jej do zmiennej u
				foreach (int v in graf[u]) { // przeglądamy listę sąsiadów
					if (kolor[v] == Kolor.Bialy) { // jeśli sąsiad nie był dotychczas odwiedzony
						kolor[v] = Kolor.Szary; // to już jest odwiedzony i jego kolor zmienia się na szary
						odleglosc[v] = odleglosc[u] + 1; // a odległość względem rodzica zwiększa się o 1
						stack.Push(v); //na stos wkładany jest element v
					}
				}
				kolor[u] = Kolor.Czarny; // kolor czarny, bo już wszystko odwiedziliśmy
			}
			return odleglosc[koniec]; // zwracamy poszukiwaną odbległość
		}
		
		// funkcja zwracająca stringa będącego opisem przemieszczanej trasy
		// algorytm przeszukiwania wszerz
		static public string Trasa(List<int>[] graf, int start, int koniec)
		{
			Kolor[] kolor = new Program.Kolor[graf.Length]; //jw
			int[] rodzic = new int[graf.Length]; // tablica ojców o długości grafu
			System.Collections.Generic.Stack<int> stack = new Stack<int>(); // stos
			for (int a = 0; a < graf.Length; ++a) { //jw.
				kolor[a] = Kolor.Bialy; //jw.
				rodzic[a] = -1; // inicjalizacja tablicy wartościami -1
			}
			kolor[start] = Kolor.Szary; // jak odwiedzony to szary
			rodzic[start] = -1; // do tablicy ojców na pozycji start ląduje -1
			stack.Push(start); // wkładamy numer wierzchołka na stos
			while (stack.Count > 0) { // mielimy do momentu aż stos się opróżni
				int u = stack.Pop(); // zdjęcie ze stosu i przypisanie do zmiennej
				foreach (int v in graf[u]) { // odwiedziny u sąsiadów
					if (kolor[v] == Kolor.Bialy) { // jeśli nie odwiedzony
						kolor[v] = Kolor.Szary; // to już odwiedzony, i poszarzony
						rodzic[v] = u; // do tablicy rodziców przypisujemy wartość u(rodzica)
						stack.Push(v); // wkładamy v na stos
					}
				}
				kolor[u] = Kolor.Czarny; // czarny bo odwiedzono wszystkie
			}
			List<int> trasa = new List<int>(); // lista do przechowywania trasy
			if (start == koniec) // dzielny dywersant nie ruszył się z miejsca
				return "";
			trasa.Add(koniec + 1); // dodanie do listy pozycje koniec+1(ostatnią pozycje)
			int b = koniec; // zmienna b zyskuje wartość zmiennej koniec
			while (true) { // pętla skończy działanie gdy b uzyska wartość zmiennej start
				b = rodzic[b]; // zmienna b otrzymuje wartość przechowywaną w tablicy rodzic na pozycji b
				trasa.Add(b + 1); // dodajemy element trasy
				if (b == start) // warunek końca jw.
					break;
			}
			trasa.Reverse(); // odwrócenie trasy aby nie iść z końca do początku
			string trasa_ = ""; // tutaj się znajdzie trasa
			foreach (int a in trasa) // przejście przez kolekcje trasa
				trasa_ += a + "-"; // klejenie stringa
			trasa_ = trasa_.Substring(0, trasa_.Length - 1); // korekta estetyczna
			return trasa_; // zwrócenie trasy w formie stringa
		}
		
		
		
		
		
		
		
		// zadanie dodatkowe
		static List<int> trasa;
		// lista intów do przechowywania drogi
		static bool koniecFunkcji = false;
		// zmienna wskazujaca czy wszystkie newralgiczne
		// krawędzie zostały odwiedzone
		
		// funkcja znajdująca przebytą trasę, za argumenty podawane są: tablica list intów - graf, oraz
		// zmienna a - pozycja startowa dywersanta, lista krotek (zawiera newralgiczne krawędzie)
		static void ZnajdzCalaTrase(List<int>[] graf, int a, List<Tuple<int,int>> newralgiczneKrawedzie)
		{
			if (!koniecFunkcji) { // jeśli nie wszystkie newralgiczne krawędzie odwiedzono
				trasa.Add(a);	// dodajemy wierzchołek do listy trasy
				// poniżej: jeśli odwiedzono wszystkie newralgiczne krawędzie
				if (WszystkieNewralgiczneKrawedzieZostalyOdwiedzone(newralgiczneKrawedzie)) {
					trasa.Remove(trasa.Last()); // usuwamy ostatni alement z trasy, nie przechodzimy przez wysadzony most
					koniecFunkcji = true; // prawda, bo odwiedzono już wszystko
				}
				// w forze iterujemy po poszczególnych krawędziach
				for (int b = 0; b < graf[a].Count && !koniecFunkcji; ++b)
					if (!trasa.Contains(graf[a][b])) // if jest po to żeby w ścieżce nie było 2 razy tego samego wierzchołka
						ZnajdzCalaTrase(graf, graf[a][b], newralgiczneKrawedzie);
				// jeżeli nie odwiedzono wszystkich newralgicznych krawędzi, wtedy z listy zawierającej trasę
				// usuwamy ostatni element
				if (!koniecFunkcji)
					trasa.Remove(trasa.Last());
			}
		}
		
		// funkcja zwraca wartość logiczną bool mówiącą czy wszystkie newralgiczne krawędzie zostały odwiedzone
		// jako argument przyjmuje listę krotek (dane newralgicznych przejść)
		static bool WszystkieNewralgiczneKrawedzieZostalyOdwiedzone(List<Tuple<int,int>> newralgiczneKrawedzie)
		{
			if (trasa.Count == 1)
				return false; // trasa zawiera jeden element, wiec nie wszystkie newralgiczne krawędzie został odwiedzone
			int znalezioneKrawedzie = 0; // licznik znalezionych krawędzi
			foreach (Tuple<int,int> krawedz in newralgiczneKrawedzie) // pętla przechodząca po krotkach z newralgicznymi krawędziami
				for (int a = 0; a < trasa.Count - 1; ++a) // pętla zagnieżdżona po wartościach trasy
				// poniżej: jeżeli conajmniej jedna z dwóch następujących po sobie krawędzi jest newralgiczne
				// wtedy zwiększami licznik znalezionych krawędzi
					if (trasa[a] == krawedz.Item1 || trasa[a + 1] == krawedz.Item2) {
						++znalezioneKrawedzie;
						break;
					}
			// funkcja zwraca wartość logiczną porównując liczbę znalezionych krawędzi z liczbą newralgicznych krawędzi
			return znalezioneKrawedzie == newralgiczneKrawedzie.Count;
		}
		
		enum Kolor
		{
			Bialy,
			Szary,
			Czarny}
		;
	}
}
